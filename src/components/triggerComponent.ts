import { Component } from "../component";

/**
 * The trigger component is useful, when something will happen in future.
 * When it is done, a given action will be performed by the game object.
 *
 * @export
 * @class TriggerComponent
 * @extends {Component}
 */
export class TriggerComponent extends Component {

    /**
     * Updates the trigger component. Inherited from Component class.
     * There is no need to call this manually.
     * @param {number} frame
     * @param {number} deltaTime
     * @memberof TriggerComponent
     */
    update(frame: number, deltaTime: number): void {
    }
}