import { Component } from "../component";

/**
 * The timer component is used, when you wish a given action to be performed once or in a loop, after a given delay.
 * @export
 * @class TimerComponent
 * @extends {Component}
 */
export class TimerComponent extends Component {
    /**
     * Updates the component. Inherited from Component class.
     * This will be called from the component class, so you do not have to call it manually.
     * @param {number} frame
     * @param {number} deltaTime
     * @return {*}  {void}
     * @memberof TimerComponent
     */
    update(frame: number, deltaTime: number): void {
    }
}