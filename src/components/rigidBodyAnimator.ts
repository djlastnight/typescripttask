import { Component } from "../component";
import { RigidBody } from "./rigidBody";

/**
 * Represents a component, used to animate a RigidBody.
 * The master game object must include a RigidBody component before instantiating an animator.
 *
 * @export
 * @class RigidBodyAnimator
 * @extends {Component}
 */
export class RigidBodyAnimator extends Component {

    /**
     * Gets the rigid body, which the current component animates.       
     *
     * @type {RigidBody}
     * @memberof RigidBodyAnimator
     */
    public readonly rigidBody: RigidBody;

    update(frame: number, deltaTime: number): void {
    }
}