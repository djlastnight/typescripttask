import { Component } from '../component';

/**
 * Rigid body represents combination of matter.js physical body and pixi.js sprite.
 * Due to it extends the Component class, it could be added to any GameObject 
 *
 * @export
 * @class RigidBody
 * @extends {Component}
 */
export class RigidBody extends Component {
  update(frame: number, deltaTime: number): void {
  }
}