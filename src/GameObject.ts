import { Component } from "./component";
import { GameObjectType } from "./gameObjectType";

/**
 * Defines a object in a game to which you can attach different components.
 *
 * @export
 * @class GameObject
 */
export class GameObject {
  /**
   * Gets the game object type.
   *
   * @type {GameObjectType}
   * @memberof GameObject
   */
  public readonly objectType: GameObjectType;

  /**
   * Gets or sets the game object tag. You can assign anything here, in case you wish the game object to carry some information with it.
   *
   * @type {Object}
   * @memberof GameObject
   */
  public tag: Object;

  private _components: Component[];

  /**
   * Gets a list with the components attached to the current game object.
   *
   * @readonly
   * @type {Component[]}
   * @memberof GameObject
   */
  public get components(): Component[] {
    return this._components;
  }

  /**
   * Creates an instance of GameObject.
   * @param {GameObjectType} type The game object type, wish you wish to create.
   * @memberof GameObject
   */
  constructor(type: GameObjectType) {
    this.objectType = type;
    this._components = new Array<Component>();
  }

  /**
   * Adds a new component to the game object.
   *
   * @param {Component} component The component to add.
   * @memberof GameObject
   */
  public addComponent(component: Component) {
    this._components.push(component);
  }

  /**
   * Removes a component from the game object, in case it is attached.
   *
   * @param {Component} component The component to detach/remove.
   * @memberof GameObject
   */
  public removeComponent(component: Component) {
    var index = this._components.indexOf(component);
    if (index != -1) {
      this._components.splice(index, 1);
    }
  }

  public getComponent<T extends Component>(): T {
    throw new Error("Method not implemented.");
  }
}
