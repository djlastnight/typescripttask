/**
 * This is the base class of all components, which might be attached to a game object.
 *
 * @export
 * @abstract
 * @class Component
 * @implements {IFrameTicker}
 */
export abstract class Component {
    public isSuspended: boolean;

    /**
     * Resumes the suspended component.
     *
     * @memberof Component
     */
    resume() {
        this.isSuspended = false;
    }

    /**
     * Suspends the component.
     *
     * @memberof Component
     */
    suspend() {
        this.isSuspended = true;
    }

    /**
     * Checks whether the component has been destroyed.
     *
     * @readonly
     * @type {boolean}
     * @memberof Component
     */
    public get isDestroyed(): boolean {
        return this.destroyed;
    }

    private frame: number;
    private updateCallback: any;

    /**
     * A value, indicating whether the component has been destroyed or not.
     *
     * @protected
     * @type {boolean}
     * @memberof Component
     */
    protected destroyed: boolean;

    /**
     * Creates an instance of Component.
     * @param {Application} app An instance of pixi.js application.
     * @memberof Component
     */
    constructor() {
        this.frame = 0;
        this.updateCallback = (dt: number) => {
            if (!this.isSuspended) {
                this.update(this.frame++, dt);
            }
        };
        
        this.destroyed = false;
    }

    /**
     * Update method is called by the pixi.js application on each frame.
     *
     * @abstract
     * @param {number} frame The current frame.
     * @param {number} deltaTime The delta time from the previous frame, measured in seconds.
     * @memberof Component
     */
    abstract update(frame: number, deltaTime: number): void;

    /**
     * Destroys the component.
     *
     * @return {*}  {void}
     * @memberof Component
     */
    public destroy(): void {
        if (this.destroyed) {
            return;
        }

        this.destroyed = true;
    }
}