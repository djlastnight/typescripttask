import { RigidBody } from "./components/rigidBody";
import { RigidBodyAnimator } from "./components/rigidBodyAnimator";
import { TimerComponent } from "./components/timerComponent";
import { TriggerComponent } from "./components/triggerComponent";
import { GameObject } from "./GameObject";
import { GameObjectType } from "./gameObjectType";

let go = new GameObject(GameObjectType.Bullet);
go.addComponent(new RigidBody());
go.addComponent(new TimerComponent());
go.addComponent(new RigidBodyAnimator());
go.addComponent(new TriggerComponent());

let timer = go.getComponent<TimerComponent>();