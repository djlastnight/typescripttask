/**
 * Defines a game object type.
 *
 * @export
 * @enum {number}
 */
export enum GameObjectType {
    None = 0,
    Hero = 1,
    Bullet = 2,
    Missile = 4,
    HostileBullet = 8,
    BirdObstacle = 16,
    Enemy = 32,
    WorldEdge = 64,
    Hearth = 128,
    EarthEdge = 256,
    Explosion = 512,
}