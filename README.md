# TypeScriptTask
to install `npm install`  
to build `./build` or `npx webpack --config webpack.config.js`  

The goal is to implement the method called at the last line of main.ts  
GameObject.getComponent<T>();

Live preview: https://djlastnight.gitlab.io/typescripttask/  