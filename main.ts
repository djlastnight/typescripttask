import { RigidBody } from "./src/components/rigidBody";
import { RigidBodyAnimator } from "./src/components/rigidBodyAnimator";
import { TimerComponent } from "./src/components/timerComponent";
import { TriggerComponent } from "./src/components/triggerComponent";
import { GameObject } from "./src/GameObject";
import { GameObjectType } from "./src/gameObjectType";


let go = new GameObject(GameObjectType.Bullet);
go.addComponent(new RigidBody());
go.addComponent(new TimerComponent());
go.addComponent(new RigidBodyAnimator());
go.addComponent(new TriggerComponent());

// This will throw exception, so you have to implement it!
let timer = go.getComponent<TimerComponent>();