/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/GameObject.ts":
/*!***************************!*\
  !*** ./src/GameObject.ts ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GameObject": () => (/* binding */ GameObject)
/* harmony export */ });
/**
 * Defines a object in a game to which you can attach different components.
 *
 * @export
 * @class GameObject
 */
var GameObject = /** @class */ (function () {
    /**
     * Creates an instance of GameObject.
     * @param {GameObjectType} type The game object type, wish you wish to create.
     * @memberof GameObject
     */
    function GameObject(type) {
        this.objectType = type;
        this._components = new Array();
    }
    Object.defineProperty(GameObject.prototype, "components", {
        /**
         * Gets a list with the components attached to the current game object.
         *
         * @readonly
         * @type {Component[]}
         * @memberof GameObject
         */
        get: function () {
            return this._components;
        },
        enumerable: false,
        configurable: true
    });
    /**
     * Adds a new component to the game object.
     *
     * @param {Component} component The component to add.
     * @memberof GameObject
     */
    GameObject.prototype.addComponent = function (component) {
        this._components.push(component);
    };
    /**
     * Removes a component from the game object, in case it is attached.
     *
     * @param {Component} component The component to detach/remove.
     * @memberof GameObject
     */
    GameObject.prototype.removeComponent = function (component) {
        var index = this._components.indexOf(component);
        if (index != -1) {
            this._components.splice(index, 1);
        }
    };
    GameObject.prototype.getComponent = function () {
        throw new Error("Method not implemented.");
    };
    return GameObject;
}());



/***/ }),

/***/ "./src/component.ts":
/*!**************************!*\
  !*** ./src/component.ts ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Component": () => (/* binding */ Component)
/* harmony export */ });
/**
 * This is the base class of all components, which might be attached to a game object.
 *
 * @export
 * @abstract
 * @class Component
 * @implements {IFrameTicker}
 */
var Component = /** @class */ (function () {
    /**
     * Creates an instance of Component.
     * @param {Application} app An instance of pixi.js application.
     * @memberof Component
     */
    function Component() {
        var _this = this;
        this.frame = 0;
        this.updateCallback = function (dt) {
            if (!_this.isSuspended) {
                _this.update(_this.frame++, dt);
            }
        };
        this.destroyed = false;
    }
    /**
     * Resumes the suspended component.
     *
     * @memberof Component
     */
    Component.prototype.resume = function () {
        this.isSuspended = false;
    };
    /**
     * Suspends the component.
     *
     * @memberof Component
     */
    Component.prototype.suspend = function () {
        this.isSuspended = true;
    };
    Object.defineProperty(Component.prototype, "isDestroyed", {
        /**
         * Checks whether the component has been destroyed.
         *
         * @readonly
         * @type {boolean}
         * @memberof Component
         */
        get: function () {
            return this.destroyed;
        },
        enumerable: false,
        configurable: true
    });
    /**
     * Destroys the component.
     *
     * @return {*}  {void}
     * @memberof Component
     */
    Component.prototype.destroy = function () {
        if (this.destroyed) {
            return;
        }
        this.destroyed = true;
    };
    return Component;
}());



/***/ }),

/***/ "./src/components/rigidBody.ts":
/*!*************************************!*\
  !*** ./src/components/rigidBody.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RigidBody": () => (/* binding */ RigidBody)
/* harmony export */ });
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../component */ "./src/component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

/**
 * Rigid body represents combination of matter.js physical body and pixi.js sprite.
 * Due to it extends the Component class, it could be added to any GameObject
 *
 * @export
 * @class RigidBody
 * @extends {Component}
 */
var RigidBody = /** @class */ (function (_super) {
    __extends(RigidBody, _super);
    function RigidBody() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RigidBody.prototype.update = function (frame, deltaTime) {
    };
    return RigidBody;
}(_component__WEBPACK_IMPORTED_MODULE_0__.Component));



/***/ }),

/***/ "./src/components/rigidBodyAnimator.ts":
/*!*********************************************!*\
  !*** ./src/components/rigidBodyAnimator.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RigidBodyAnimator": () => (/* binding */ RigidBodyAnimator)
/* harmony export */ });
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../component */ "./src/component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

/**
 * Represents a component, used to animate a RigidBody.
 * The master game object must include a RigidBody component before instantiating an animator.
 *
 * @export
 * @class RigidBodyAnimator
 * @extends {Component}
 */
var RigidBodyAnimator = /** @class */ (function (_super) {
    __extends(RigidBodyAnimator, _super);
    function RigidBodyAnimator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RigidBodyAnimator.prototype.update = function (frame, deltaTime) {
    };
    return RigidBodyAnimator;
}(_component__WEBPACK_IMPORTED_MODULE_0__.Component));



/***/ }),

/***/ "./src/components/timerComponent.ts":
/*!******************************************!*\
  !*** ./src/components/timerComponent.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TimerComponent": () => (/* binding */ TimerComponent)
/* harmony export */ });
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../component */ "./src/component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

/**
 * The timer component is used, when you wish a given action to be performed once or in a loop, after a given delay.
 * @export
 * @class TimerComponent
 * @extends {Component}
 */
var TimerComponent = /** @class */ (function (_super) {
    __extends(TimerComponent, _super);
    function TimerComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Updates the component. Inherited from Component class.
     * This will be called from the component class, so you do not have to call it manually.
     * @param {number} frame
     * @param {number} deltaTime
     * @return {*}  {void}
     * @memberof TimerComponent
     */
    TimerComponent.prototype.update = function (frame, deltaTime) {
    };
    return TimerComponent;
}(_component__WEBPACK_IMPORTED_MODULE_0__.Component));



/***/ }),

/***/ "./src/components/triggerComponent.ts":
/*!********************************************!*\
  !*** ./src/components/triggerComponent.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TriggerComponent": () => (/* binding */ TriggerComponent)
/* harmony export */ });
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../component */ "./src/component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

/**
 * The trigger component is useful, when something will happen in future.
 * When it is done, a given action will be performed by the game object.
 *
 * @export
 * @class TriggerComponent
 * @extends {Component}
 */
var TriggerComponent = /** @class */ (function (_super) {
    __extends(TriggerComponent, _super);
    function TriggerComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Updates the trigger component. Inherited from Component class.
     * There is no need to call this manually.
     * @param {number} frame
     * @param {number} deltaTime
     * @memberof TriggerComponent
     */
    TriggerComponent.prototype.update = function (frame, deltaTime) {
    };
    return TriggerComponent;
}(_component__WEBPACK_IMPORTED_MODULE_0__.Component));



/***/ }),

/***/ "./src/gameObjectType.ts":
/*!*******************************!*\
  !*** ./src/gameObjectType.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GameObjectType": () => (/* binding */ GameObjectType)
/* harmony export */ });
/**
 * Defines a game object type.
 *
 * @export
 * @enum {number}
 */
var GameObjectType;
(function (GameObjectType) {
    GameObjectType[GameObjectType["None"] = 0] = "None";
    GameObjectType[GameObjectType["Hero"] = 1] = "Hero";
    GameObjectType[GameObjectType["Bullet"] = 2] = "Bullet";
    GameObjectType[GameObjectType["Missile"] = 4] = "Missile";
    GameObjectType[GameObjectType["HostileBullet"] = 8] = "HostileBullet";
    GameObjectType[GameObjectType["BirdObstacle"] = 16] = "BirdObstacle";
    GameObjectType[GameObjectType["Enemy"] = 32] = "Enemy";
    GameObjectType[GameObjectType["WorldEdge"] = 64] = "WorldEdge";
    GameObjectType[GameObjectType["Hearth"] = 128] = "Hearth";
    GameObjectType[GameObjectType["EarthEdge"] = 256] = "EarthEdge";
    GameObjectType[GameObjectType["Explosion"] = 512] = "Explosion";
})(GameObjectType || (GameObjectType = {}));


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_rigidBody__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/rigidBody */ "./src/components/rigidBody.ts");
/* harmony import */ var _components_rigidBodyAnimator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/rigidBodyAnimator */ "./src/components/rigidBodyAnimator.ts");
/* harmony import */ var _components_timerComponent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/timerComponent */ "./src/components/timerComponent.ts");
/* harmony import */ var _components_triggerComponent__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/triggerComponent */ "./src/components/triggerComponent.ts");
/* harmony import */ var _GameObject__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./GameObject */ "./src/GameObject.ts");
/* harmony import */ var _gameObjectType__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gameObjectType */ "./src/gameObjectType.ts");






var go = new _GameObject__WEBPACK_IMPORTED_MODULE_4__.GameObject(_gameObjectType__WEBPACK_IMPORTED_MODULE_5__.GameObjectType.Bullet);
go.addComponent(new _components_rigidBody__WEBPACK_IMPORTED_MODULE_0__.RigidBody());
go.addComponent(new _components_timerComponent__WEBPACK_IMPORTED_MODULE_2__.TimerComponent());
go.addComponent(new _components_rigidBodyAnimator__WEBPACK_IMPORTED_MODULE_1__.RigidBodyAnimator());
go.addComponent(new _components_triggerComponent__WEBPACK_IMPORTED_MODULE_3__.TriggerComponent());
var timer = go.getComponent();

})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUdBOzs7OztHQUtHO0FBQ0g7SUE4QkU7Ozs7T0FJRztJQUNILG9CQUFZLElBQW9CO1FBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxLQUFLLEVBQWEsQ0FBQztJQUM1QyxDQUFDO0lBWkQsc0JBQVcsa0NBQVU7UUFQckI7Ozs7OztXQU1HO2FBQ0g7WUFDRSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDMUIsQ0FBQzs7O09BQUE7SUFZRDs7Ozs7T0FLRztJQUNJLGlDQUFZLEdBQW5CLFVBQW9CLFNBQW9CO1FBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNJLG9DQUFlLEdBQXRCLFVBQXVCLFNBQW9CO1FBQ3pDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2hELElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxFQUFFO1lBQ2YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ25DO0lBQ0gsQ0FBQztJQUVNLGlDQUFZLEdBQW5CO1FBQ0UsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFDSCxpQkFBQztBQUFELENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzRUQ7Ozs7Ozs7R0FPRztBQUNIO0lBNENJOzs7O09BSUc7SUFDSDtRQUFBLGlCQVNDO1FBUkcsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZixJQUFJLENBQUMsY0FBYyxHQUFHLFVBQUMsRUFBVTtZQUM3QixJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRTtnQkFDbkIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDakM7UUFDTCxDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBdkREOzs7O09BSUc7SUFDSCwwQkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCwyQkFBTyxHQUFQO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7SUFDNUIsQ0FBQztJQVNELHNCQUFXLGtDQUFXO1FBUHRCOzs7Ozs7V0FNRzthQUNIO1lBQ0ksT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFCLENBQUM7OztPQUFBO0lBd0NEOzs7OztPQUtHO0lBQ0ksMkJBQU8sR0FBZDtRQUNJLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixPQUFPO1NBQ1Y7UUFFRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBQ0wsZ0JBQUM7QUFBRCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNGd0M7QUFFekM7Ozs7Ozs7R0FPRztBQUNIO0lBQStCLDZCQUFTO0lBQXhDOztJQUdBLENBQUM7SUFGQywwQkFBTSxHQUFOLFVBQU8sS0FBYSxFQUFFLFNBQWlCO0lBQ3ZDLENBQUM7SUFDSCxnQkFBQztBQUFELENBQUMsQ0FIOEIsaURBQVMsR0FHdkM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDYndDO0FBR3pDOzs7Ozs7O0dBT0c7QUFDSDtJQUF1QyxxQ0FBUztJQUFoRDs7SUFZQSxDQUFDO0lBRkcsa0NBQU0sR0FBTixVQUFPLEtBQWEsRUFBRSxTQUFpQjtJQUN2QyxDQUFDO0lBQ0wsd0JBQUM7QUFBRCxDQUFDLENBWnNDLGlEQUFTLEdBWS9DOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZCd0M7QUFFekM7Ozs7O0dBS0c7QUFDSDtJQUFvQyxrQ0FBUztJQUE3Qzs7SUFXQSxDQUFDO0lBVkc7Ozs7Ozs7T0FPRztJQUNILCtCQUFNLEdBQU4sVUFBTyxLQUFhLEVBQUUsU0FBaUI7SUFDdkMsQ0FBQztJQUNMLHFCQUFDO0FBQUQsQ0FBQyxDQVhtQyxpREFBUyxHQVc1Qzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuQndDO0FBRXpDOzs7Ozs7O0dBT0c7QUFDSDtJQUFzQyxvQ0FBUztJQUEvQzs7SUFXQSxDQUFDO0lBVEc7Ozs7OztPQU1HO0lBQ0gsaUNBQU0sR0FBTixVQUFPLEtBQWEsRUFBRSxTQUFpQjtJQUN2QyxDQUFDO0lBQ0wsdUJBQUM7QUFBRCxDQUFDLENBWHFDLGlEQUFTLEdBVzlDOzs7Ozs7Ozs7Ozs7Ozs7O0FDckJEOzs7OztHQUtHO0FBQ0gsSUFBWSxjQVlYO0FBWkQsV0FBWSxjQUFjO0lBQ3RCLG1EQUFRO0lBQ1IsbURBQVE7SUFDUix1REFBVTtJQUNWLHlEQUFXO0lBQ1gscUVBQWlCO0lBQ2pCLG9FQUFpQjtJQUNqQixzREFBVTtJQUNWLDhEQUFjO0lBQ2QseURBQVk7SUFDWiwrREFBZTtJQUNmLCtEQUFlO0FBQ25CLENBQUMsRUFaVyxjQUFjLEtBQWQsY0FBYyxRQVl6Qjs7Ozs7OztVQ2xCRDtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7OztXQ3RCQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLHlDQUF5Qyx3Q0FBd0M7V0FDakY7V0FDQTtXQUNBOzs7OztXQ1BBOzs7OztXQ0FBO1dBQ0E7V0FDQTtXQUNBLHVEQUF1RCxpQkFBaUI7V0FDeEU7V0FDQSxnREFBZ0QsYUFBYTtXQUM3RDs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNObUQ7QUFDZ0I7QUFDTjtBQUNJO0FBQ3ZCO0FBQ1E7QUFFbEQsSUFBSSxFQUFFLEdBQUcsSUFBSSxtREFBVSxDQUFDLGtFQUFxQixDQUFDLENBQUM7QUFDL0MsRUFBRSxDQUFDLFlBQVksQ0FBQyxJQUFJLDREQUFTLEVBQUUsQ0FBQyxDQUFDO0FBQ2pDLEVBQUUsQ0FBQyxZQUFZLENBQUMsSUFBSSxzRUFBYyxFQUFFLENBQUMsQ0FBQztBQUN0QyxFQUFFLENBQUMsWUFBWSxDQUFDLElBQUksNEVBQWlCLEVBQUUsQ0FBQyxDQUFDO0FBQ3pDLEVBQUUsQ0FBQyxZQUFZLENBQUMsSUFBSSwwRUFBZ0IsRUFBRSxDQUFDLENBQUM7QUFFeEMsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLFlBQVksRUFBa0IsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovL3R5cGVzY3JpcHR0YXNrLy4vc3JjL0dhbWVPYmplY3QudHMiLCJ3ZWJwYWNrOi8vdHlwZXNjcmlwdHRhc2svLi9zcmMvY29tcG9uZW50LnRzIiwid2VicGFjazovL3R5cGVzY3JpcHR0YXNrLy4vc3JjL2NvbXBvbmVudHMvcmlnaWRCb2R5LnRzIiwid2VicGFjazovL3R5cGVzY3JpcHR0YXNrLy4vc3JjL2NvbXBvbmVudHMvcmlnaWRCb2R5QW5pbWF0b3IudHMiLCJ3ZWJwYWNrOi8vdHlwZXNjcmlwdHRhc2svLi9zcmMvY29tcG9uZW50cy90aW1lckNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly90eXBlc2NyaXB0dGFzay8uL3NyYy9jb21wb25lbnRzL3RyaWdnZXJDb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vdHlwZXNjcmlwdHRhc2svLi9zcmMvZ2FtZU9iamVjdFR5cGUudHMiLCJ3ZWJwYWNrOi8vdHlwZXNjcmlwdHRhc2svd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vdHlwZXNjcmlwdHRhc2svd2VicGFjay9ydW50aW1lL2RlZmluZSBwcm9wZXJ0eSBnZXR0ZXJzIiwid2VicGFjazovL3R5cGVzY3JpcHR0YXNrL3dlYnBhY2svcnVudGltZS9oYXNPd25Qcm9wZXJ0eSBzaG9ydGhhbmQiLCJ3ZWJwYWNrOi8vdHlwZXNjcmlwdHRhc2svd2VicGFjay9ydW50aW1lL21ha2UgbmFtZXNwYWNlIG9iamVjdCIsIndlYnBhY2s6Ly90eXBlc2NyaXB0dGFzay8uL3NyYy9tYWluLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCIuL2NvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBHYW1lT2JqZWN0VHlwZSB9IGZyb20gXCIuL2dhbWVPYmplY3RUeXBlXCI7XHJcblxyXG4vKipcclxuICogRGVmaW5lcyBhIG9iamVjdCBpbiBhIGdhbWUgdG8gd2hpY2ggeW91IGNhbiBhdHRhY2ggZGlmZmVyZW50IGNvbXBvbmVudHMuXHJcbiAqXHJcbiAqIEBleHBvcnRcclxuICogQGNsYXNzIEdhbWVPYmplY3RcclxuICovXHJcbmV4cG9ydCBjbGFzcyBHYW1lT2JqZWN0IHtcclxuICAvKipcclxuICAgKiBHZXRzIHRoZSBnYW1lIG9iamVjdCB0eXBlLlxyXG4gICAqXHJcbiAgICogQHR5cGUge0dhbWVPYmplY3RUeXBlfVxyXG4gICAqIEBtZW1iZXJvZiBHYW1lT2JqZWN0XHJcbiAgICovXHJcbiAgcHVibGljIHJlYWRvbmx5IG9iamVjdFR5cGU6IEdhbWVPYmplY3RUeXBlO1xyXG5cclxuICAvKipcclxuICAgKiBHZXRzIG9yIHNldHMgdGhlIGdhbWUgb2JqZWN0IHRhZy4gWW91IGNhbiBhc3NpZ24gYW55dGhpbmcgaGVyZSwgaW4gY2FzZSB5b3Ugd2lzaCB0aGUgZ2FtZSBvYmplY3QgdG8gY2Fycnkgc29tZSBpbmZvcm1hdGlvbiB3aXRoIGl0LlxyXG4gICAqXHJcbiAgICogQHR5cGUge09iamVjdH1cclxuICAgKiBAbWVtYmVyb2YgR2FtZU9iamVjdFxyXG4gICAqL1xyXG4gIHB1YmxpYyB0YWc6IE9iamVjdDtcclxuXHJcbiAgcHJpdmF0ZSBfY29tcG9uZW50czogQ29tcG9uZW50W107XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldHMgYSBsaXN0IHdpdGggdGhlIGNvbXBvbmVudHMgYXR0YWNoZWQgdG8gdGhlIGN1cnJlbnQgZ2FtZSBvYmplY3QuXHJcbiAgICpcclxuICAgKiBAcmVhZG9ubHlcclxuICAgKiBAdHlwZSB7Q29tcG9uZW50W119XHJcbiAgICogQG1lbWJlcm9mIEdhbWVPYmplY3RcclxuICAgKi9cclxuICBwdWJsaWMgZ2V0IGNvbXBvbmVudHMoKTogQ29tcG9uZW50W10ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2NvbXBvbmVudHM7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBDcmVhdGVzIGFuIGluc3RhbmNlIG9mIEdhbWVPYmplY3QuXHJcbiAgICogQHBhcmFtIHtHYW1lT2JqZWN0VHlwZX0gdHlwZSBUaGUgZ2FtZSBvYmplY3QgdHlwZSwgd2lzaCB5b3Ugd2lzaCB0byBjcmVhdGUuXHJcbiAgICogQG1lbWJlcm9mIEdhbWVPYmplY3RcclxuICAgKi9cclxuICBjb25zdHJ1Y3Rvcih0eXBlOiBHYW1lT2JqZWN0VHlwZSkge1xyXG4gICAgdGhpcy5vYmplY3RUeXBlID0gdHlwZTtcclxuICAgIHRoaXMuX2NvbXBvbmVudHMgPSBuZXcgQXJyYXk8Q29tcG9uZW50PigpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQWRkcyBhIG5ldyBjb21wb25lbnQgdG8gdGhlIGdhbWUgb2JqZWN0LlxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtDb21wb25lbnR9IGNvbXBvbmVudCBUaGUgY29tcG9uZW50IHRvIGFkZC5cclxuICAgKiBAbWVtYmVyb2YgR2FtZU9iamVjdFxyXG4gICAqL1xyXG4gIHB1YmxpYyBhZGRDb21wb25lbnQoY29tcG9uZW50OiBDb21wb25lbnQpIHtcclxuICAgIHRoaXMuX2NvbXBvbmVudHMucHVzaChjb21wb25lbnQpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUmVtb3ZlcyBhIGNvbXBvbmVudCBmcm9tIHRoZSBnYW1lIG9iamVjdCwgaW4gY2FzZSBpdCBpcyBhdHRhY2hlZC5cclxuICAgKlxyXG4gICAqIEBwYXJhbSB7Q29tcG9uZW50fSBjb21wb25lbnQgVGhlIGNvbXBvbmVudCB0byBkZXRhY2gvcmVtb3ZlLlxyXG4gICAqIEBtZW1iZXJvZiBHYW1lT2JqZWN0XHJcbiAgICovXHJcbiAgcHVibGljIHJlbW92ZUNvbXBvbmVudChjb21wb25lbnQ6IENvbXBvbmVudCkge1xyXG4gICAgdmFyIGluZGV4ID0gdGhpcy5fY29tcG9uZW50cy5pbmRleE9mKGNvbXBvbmVudCk7XHJcbiAgICBpZiAoaW5kZXggIT0gLTEpIHtcclxuICAgICAgdGhpcy5fY29tcG9uZW50cy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldENvbXBvbmVudDxUIGV4dGVuZHMgQ29tcG9uZW50PigpOiBUIHtcclxuICAgIHRocm93IG5ldyBFcnJvcihcIk1ldGhvZCBub3QgaW1wbGVtZW50ZWQuXCIpO1xyXG4gIH1cclxufVxyXG4iLCIvKipcclxuICogVGhpcyBpcyB0aGUgYmFzZSBjbGFzcyBvZiBhbGwgY29tcG9uZW50cywgd2hpY2ggbWlnaHQgYmUgYXR0YWNoZWQgdG8gYSBnYW1lIG9iamVjdC5cclxuICpcclxuICogQGV4cG9ydFxyXG4gKiBAYWJzdHJhY3RcclxuICogQGNsYXNzIENvbXBvbmVudFxyXG4gKiBAaW1wbGVtZW50cyB7SUZyYW1lVGlja2VyfVxyXG4gKi9cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIENvbXBvbmVudCB7XHJcbiAgICBwdWJsaWMgaXNTdXNwZW5kZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXN1bWVzIHRoZSBzdXNwZW5kZWQgY29tcG9uZW50LlxyXG4gICAgICpcclxuICAgICAqIEBtZW1iZXJvZiBDb21wb25lbnRcclxuICAgICAqL1xyXG4gICAgcmVzdW1lKCkge1xyXG4gICAgICAgIHRoaXMuaXNTdXNwZW5kZWQgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFN1c3BlbmRzIHRoZSBjb21wb25lbnQuXHJcbiAgICAgKlxyXG4gICAgICogQG1lbWJlcm9mIENvbXBvbmVudFxyXG4gICAgICovXHJcbiAgICBzdXNwZW5kKCkge1xyXG4gICAgICAgIHRoaXMuaXNTdXNwZW5kZWQgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIHdoZXRoZXIgdGhlIGNvbXBvbmVudCBoYXMgYmVlbiBkZXN0cm95ZWQuXHJcbiAgICAgKlxyXG4gICAgICogQHJlYWRvbmx5XHJcbiAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cclxuICAgICAqIEBtZW1iZXJvZiBDb21wb25lbnRcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBpc0Rlc3Ryb3llZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kZXN0cm95ZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBmcmFtZTogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSB1cGRhdGVDYWxsYmFjazogYW55O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQSB2YWx1ZSwgaW5kaWNhdGluZyB3aGV0aGVyIHRoZSBjb21wb25lbnQgaGFzIGJlZW4gZGVzdHJveWVkIG9yIG5vdC5cclxuICAgICAqXHJcbiAgICAgKiBAcHJvdGVjdGVkXHJcbiAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cclxuICAgICAqIEBtZW1iZXJvZiBDb21wb25lbnRcclxuICAgICAqL1xyXG4gICAgcHJvdGVjdGVkIGRlc3Ryb3llZDogYm9vbGVhbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYW4gaW5zdGFuY2Ugb2YgQ29tcG9uZW50LlxyXG4gICAgICogQHBhcmFtIHtBcHBsaWNhdGlvbn0gYXBwIEFuIGluc3RhbmNlIG9mIHBpeGkuanMgYXBwbGljYXRpb24uXHJcbiAgICAgKiBAbWVtYmVyb2YgQ29tcG9uZW50XHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuZnJhbWUgPSAwO1xyXG4gICAgICAgIHRoaXMudXBkYXRlQ2FsbGJhY2sgPSAoZHQ6IG51bWJlcikgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNTdXNwZW5kZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlKHRoaXMuZnJhbWUrKywgZHQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLmRlc3Ryb3llZCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVXBkYXRlIG1ldGhvZCBpcyBjYWxsZWQgYnkgdGhlIHBpeGkuanMgYXBwbGljYXRpb24gb24gZWFjaCBmcmFtZS5cclxuICAgICAqXHJcbiAgICAgKiBAYWJzdHJhY3RcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBmcmFtZSBUaGUgY3VycmVudCBmcmFtZS5cclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBkZWx0YVRpbWUgVGhlIGRlbHRhIHRpbWUgZnJvbSB0aGUgcHJldmlvdXMgZnJhbWUsIG1lYXN1cmVkIGluIHNlY29uZHMuXHJcbiAgICAgKiBAbWVtYmVyb2YgQ29tcG9uZW50XHJcbiAgICAgKi9cclxuICAgIGFic3RyYWN0IHVwZGF0ZShmcmFtZTogbnVtYmVyLCBkZWx0YVRpbWU6IG51bWJlcik6IHZvaWQ7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXN0cm95cyB0aGUgY29tcG9uZW50LlxyXG4gICAgICpcclxuICAgICAqIEByZXR1cm4geyp9ICB7dm9pZH1cclxuICAgICAqIEBtZW1iZXJvZiBDb21wb25lbnRcclxuICAgICAqL1xyXG4gICAgcHVibGljIGRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGVzdHJveWVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZGVzdHJveWVkID0gdHJ1ZTtcclxuICAgIH1cclxufSIsImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJy4uL2NvbXBvbmVudCc7XHJcblxyXG4vKipcclxuICogUmlnaWQgYm9keSByZXByZXNlbnRzIGNvbWJpbmF0aW9uIG9mIG1hdHRlci5qcyBwaHlzaWNhbCBib2R5IGFuZCBwaXhpLmpzIHNwcml0ZS5cclxuICogRHVlIHRvIGl0IGV4dGVuZHMgdGhlIENvbXBvbmVudCBjbGFzcywgaXQgY291bGQgYmUgYWRkZWQgdG8gYW55IEdhbWVPYmplY3QgXHJcbiAqXHJcbiAqIEBleHBvcnRcclxuICogQGNsYXNzIFJpZ2lkQm9keVxyXG4gKiBAZXh0ZW5kcyB7Q29tcG9uZW50fVxyXG4gKi9cclxuZXhwb3J0IGNsYXNzIFJpZ2lkQm9keSBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgdXBkYXRlKGZyYW1lOiBudW1iZXIsIGRlbHRhVGltZTogbnVtYmVyKTogdm9pZCB7XHJcbiAgfVxyXG59IiwiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIi4uL2NvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBSaWdpZEJvZHkgfSBmcm9tIFwiLi9yaWdpZEJvZHlcIjtcclxuXHJcbi8qKlxyXG4gKiBSZXByZXNlbnRzIGEgY29tcG9uZW50LCB1c2VkIHRvIGFuaW1hdGUgYSBSaWdpZEJvZHkuXHJcbiAqIFRoZSBtYXN0ZXIgZ2FtZSBvYmplY3QgbXVzdCBpbmNsdWRlIGEgUmlnaWRCb2R5IGNvbXBvbmVudCBiZWZvcmUgaW5zdGFudGlhdGluZyBhbiBhbmltYXRvci5cclxuICpcclxuICogQGV4cG9ydFxyXG4gKiBAY2xhc3MgUmlnaWRCb2R5QW5pbWF0b3JcclxuICogQGV4dGVuZHMge0NvbXBvbmVudH1cclxuICovXHJcbmV4cG9ydCBjbGFzcyBSaWdpZEJvZHlBbmltYXRvciBleHRlbmRzIENvbXBvbmVudCB7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSByaWdpZCBib2R5LCB3aGljaCB0aGUgY3VycmVudCBjb21wb25lbnQgYW5pbWF0ZXMuICAgICAgIFxyXG4gICAgICpcclxuICAgICAqIEB0eXBlIHtSaWdpZEJvZHl9XHJcbiAgICAgKiBAbWVtYmVyb2YgUmlnaWRCb2R5QW5pbWF0b3JcclxuICAgICAqL1xyXG4gICAgcHVibGljIHJlYWRvbmx5IHJpZ2lkQm9keTogUmlnaWRCb2R5O1xyXG5cclxuICAgIHVwZGF0ZShmcmFtZTogbnVtYmVyLCBkZWx0YVRpbWU6IG51bWJlcik6IHZvaWQge1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIi4uL2NvbXBvbmVudFwiO1xyXG5cclxuLyoqXHJcbiAqIFRoZSB0aW1lciBjb21wb25lbnQgaXMgdXNlZCwgd2hlbiB5b3Ugd2lzaCBhIGdpdmVuIGFjdGlvbiB0byBiZSBwZXJmb3JtZWQgb25jZSBvciBpbiBhIGxvb3AsIGFmdGVyIGEgZ2l2ZW4gZGVsYXkuXHJcbiAqIEBleHBvcnRcclxuICogQGNsYXNzIFRpbWVyQ29tcG9uZW50XHJcbiAqIEBleHRlbmRzIHtDb21wb25lbnR9XHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgVGltZXJDb21wb25lbnQgZXh0ZW5kcyBDb21wb25lbnQge1xyXG4gICAgLyoqXHJcbiAgICAgKiBVcGRhdGVzIHRoZSBjb21wb25lbnQuIEluaGVyaXRlZCBmcm9tIENvbXBvbmVudCBjbGFzcy5cclxuICAgICAqIFRoaXMgd2lsbCBiZSBjYWxsZWQgZnJvbSB0aGUgY29tcG9uZW50IGNsYXNzLCBzbyB5b3UgZG8gbm90IGhhdmUgdG8gY2FsbCBpdCBtYW51YWxseS5cclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBmcmFtZVxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGRlbHRhVGltZVxyXG4gICAgICogQHJldHVybiB7Kn0gIHt2b2lkfVxyXG4gICAgICogQG1lbWJlcm9mIFRpbWVyQ29tcG9uZW50XHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZShmcmFtZTogbnVtYmVyLCBkZWx0YVRpbWU6IG51bWJlcik6IHZvaWQge1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIi4uL2NvbXBvbmVudFwiO1xyXG5cclxuLyoqXHJcbiAqIFRoZSB0cmlnZ2VyIGNvbXBvbmVudCBpcyB1c2VmdWwsIHdoZW4gc29tZXRoaW5nIHdpbGwgaGFwcGVuIGluIGZ1dHVyZS5cclxuICogV2hlbiBpdCBpcyBkb25lLCBhIGdpdmVuIGFjdGlvbiB3aWxsIGJlIHBlcmZvcm1lZCBieSB0aGUgZ2FtZSBvYmplY3QuXHJcbiAqXHJcbiAqIEBleHBvcnRcclxuICogQGNsYXNzIFRyaWdnZXJDb21wb25lbnRcclxuICogQGV4dGVuZHMge0NvbXBvbmVudH1cclxuICovXHJcbmV4cG9ydCBjbGFzcyBUcmlnZ2VyQ29tcG9uZW50IGV4dGVuZHMgQ29tcG9uZW50IHtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFVwZGF0ZXMgdGhlIHRyaWdnZXIgY29tcG9uZW50LiBJbmhlcml0ZWQgZnJvbSBDb21wb25lbnQgY2xhc3MuXHJcbiAgICAgKiBUaGVyZSBpcyBubyBuZWVkIHRvIGNhbGwgdGhpcyBtYW51YWxseS5cclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBmcmFtZVxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGRlbHRhVGltZVxyXG4gICAgICogQG1lbWJlcm9mIFRyaWdnZXJDb21wb25lbnRcclxuICAgICAqL1xyXG4gICAgdXBkYXRlKGZyYW1lOiBudW1iZXIsIGRlbHRhVGltZTogbnVtYmVyKTogdm9pZCB7XHJcbiAgICB9XHJcbn0iLCIvKipcclxuICogRGVmaW5lcyBhIGdhbWUgb2JqZWN0IHR5cGUuXHJcbiAqXHJcbiAqIEBleHBvcnRcclxuICogQGVudW0ge251bWJlcn1cclxuICovXHJcbmV4cG9ydCBlbnVtIEdhbWVPYmplY3RUeXBlIHtcclxuICAgIE5vbmUgPSAwLFxyXG4gICAgSGVybyA9IDEsXHJcbiAgICBCdWxsZXQgPSAyLFxyXG4gICAgTWlzc2lsZSA9IDQsXHJcbiAgICBIb3N0aWxlQnVsbGV0ID0gOCxcclxuICAgIEJpcmRPYnN0YWNsZSA9IDE2LFxyXG4gICAgRW5lbXkgPSAzMixcclxuICAgIFdvcmxkRWRnZSA9IDY0LFxyXG4gICAgSGVhcnRoID0gMTI4LFxyXG4gICAgRWFydGhFZGdlID0gMjU2LFxyXG4gICAgRXhwbG9zaW9uID0gNTEyLFxyXG59IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb25zIGZvciBoYXJtb255IGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uZCA9IChleHBvcnRzLCBkZWZpbml0aW9uKSA9PiB7XG5cdGZvcih2YXIga2V5IGluIGRlZmluaXRpb24pIHtcblx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZGVmaW5pdGlvbiwga2V5KSAmJiAhX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIGtleSkpIHtcblx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBrZXksIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBkZWZpbml0aW9uW2tleV0gfSk7XG5cdFx0fVxuXHR9XG59OyIsIl9fd2VicGFja19yZXF1aXJlX18ubyA9IChvYmosIHByb3ApID0+IChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKSkiLCIvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSAoZXhwb3J0cykgPT4ge1xuXHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcblx0fVxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xufTsiLCJpbXBvcnQgeyBSaWdpZEJvZHkgfSBmcm9tIFwiLi9jb21wb25lbnRzL3JpZ2lkQm9keVwiO1xyXG5pbXBvcnQgeyBSaWdpZEJvZHlBbmltYXRvciB9IGZyb20gXCIuL2NvbXBvbmVudHMvcmlnaWRCb2R5QW5pbWF0b3JcIjtcclxuaW1wb3J0IHsgVGltZXJDb21wb25lbnQgfSBmcm9tIFwiLi9jb21wb25lbnRzL3RpbWVyQ29tcG9uZW50XCI7XHJcbmltcG9ydCB7IFRyaWdnZXJDb21wb25lbnQgfSBmcm9tIFwiLi9jb21wb25lbnRzL3RyaWdnZXJDb21wb25lbnRcIjtcclxuaW1wb3J0IHsgR2FtZU9iamVjdCB9IGZyb20gXCIuL0dhbWVPYmplY3RcIjtcclxuaW1wb3J0IHsgR2FtZU9iamVjdFR5cGUgfSBmcm9tIFwiLi9nYW1lT2JqZWN0VHlwZVwiO1xyXG5cclxubGV0IGdvID0gbmV3IEdhbWVPYmplY3QoR2FtZU9iamVjdFR5cGUuQnVsbGV0KTtcclxuZ28uYWRkQ29tcG9uZW50KG5ldyBSaWdpZEJvZHkoKSk7XHJcbmdvLmFkZENvbXBvbmVudChuZXcgVGltZXJDb21wb25lbnQoKSk7XHJcbmdvLmFkZENvbXBvbmVudChuZXcgUmlnaWRCb2R5QW5pbWF0b3IoKSk7XHJcbmdvLmFkZENvbXBvbmVudChuZXcgVHJpZ2dlckNvbXBvbmVudCgpKTtcclxuXHJcbmxldCB0aW1lciA9IGdvLmdldENvbXBvbmVudDxUaW1lckNvbXBvbmVudD4oKTsiXSwibmFtZXMiOltdLCJzb3VyY2VSb290IjoiIn0=